# TP4 : Buffet à volonté

## Ma configuration GNS3

![conf_gns3](images/confgns3.png)  

*désole c'est une image*

## Automatisation par fichier texte

### Configuration des clients switch

Le fichier texte `conf_vlan_iou.txt` définis les vlan des client-switch.  
Le fichier texte `conf_access_iou.txt` configure le mode access des client-switch.  
Le fichier texte `conf_trunk_iou.txt` configure le mode trunk entre les client-switch. Étant donné que les ports sont différents, le fichier est réparti de manière à automatiser convenablement les bons switch.  

### Configuration de l'infra switch

On retrouve la configuration à faire dans les même fichiers texte que pour les clients. Faut pas se tromper dans les copier-coller juste.

### Configuration de la liaison R1

Automatisation via le fichier texte `conf_r1.txt`.

```bash
R1#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet1/0            192.168.122.72  YES DHCP   up                    up      
FastEthernet2/0            unassigned      YES NVRAM  up                    up      
FastEthernet2/0.30         10.5.30.254     YES NVRAM  up                    up      
FastEthernet3/0            unassigned      YES NVRAM  up                    up      
FastEthernet3/0.10         10.5.10.254     YES NVRAM  up                    up      
FastEthernet3/0.20         10.5.20.254     YES NVRAM  up                    up
```

## Test de la configuration

### Configuration de la zone A 

Logiquement, chaque admins peut se ping ainsi que chaque guests. Et logiquement, un admin ne peut pas ping de guests car ils ne sont pas dans le même vlan.  
Je note tout de même que le protocole **spanning-tree** s'est déclenché entre le client-sw2 et le client-sw3, ce qui pour moi n'est pas logique encore une fois.  
```bash
client-sw2#sh int tr

Port        Mode             Encapsulation  Status        Native vlan
Et3/1       on               802.1q         trunking      1
Et3/2       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et3/1       10,20
Et3/2       10,20

Port        Vlans allowed and active in management domain
Et3/0       10,20
Et3/1       10,20
Et3/2       10,20

Port        Vlans in spanning tree forwarding state and not pruned
Et3/1       10,20
Et3/2       none
```  

Configuration d'admin1 :

```bash
admin1> sh ip

NAME        : admin1[1]
IP/MASK     : 10.5.10.11/24
GATEWAY     : 10.5.10.254
DNS         : 
MAC         : 00:50:79:66:68:04
LPORT       : 20031
RHOST:PORT  : 127.0.0.1:20032
MTU:        : 1500
```

Maintenant voici admin1 qui tente de ping en premier admin2 puis admin3 :  
```bash
admin1> ping 10.5.10.12
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.473 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=0.575 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=0.597 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=0.719 ms
84 bytes from 10.5.10.12 icmp_seq=5 ttl=64 time=0.650 ms
^C
admin1> ping 10.5.10.13
host (10.5.10.13) not reachable
```  
Même chose pour guest1, le ping est positif vers guest2 mais vers guest3, **nouvel échec**.

En revérifiant les client-sw avec `sw int tr`, je remarque que le protocole spanning-tree s'est *désactivé*... Pourquoi pas...  
```bash
client-sw2#sh int tr
...

Port        Vlans in spanning tree forwarding state and not pruned
Et3/0       10,20
Et3/1       10,20
Et3/2       10,20
```
Cependant ça n'affecte pas les admins1-2 et les guests1-2 à pouvoir ping admin3 et guest3.  

### Configuration de la zone B

Sur infra-sw1, on retrouve le vlan30.
```bash
infra-sw1#sh vl

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/0, Et0/1, Et0/2, Et0/3
                                                Et1/0, Et1/3, Et2/1, Et2/2
                                                Et2/3, Et3/0, Et3/1, Et3/2
                                                Et3/3
10   admins                           active    
20   guests                           active    
30   infra                            active    Et1/1, Et1/2
infra-sw1#sh int tr

Port        Mode             Encapsulation  Status        Native vlan
Et2/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et2/0       30

Port        Vlans allowed and active in management domain
Et2/0       30

Port        Vlans in spanning tree forwarding state and not pruned
Et2/0       none
```
Oui, le spanning-tree est actif......  
**(╯°□°）╯︵ ┻━┻**   
Il s'est désactivé par la suite.  
```bash
infra-sw1#sh int tr

...
Port        Vlans in spanning tree forwarding state and not pruned
Et2/0       30
```
**┬─┬ノ( ゜-゜ノ)**  
**┬─┬⃰͡ (ᵔᵕᵔ͜ )**
Merci la solution de conf de gns3 mais ça me daille de pas y arriver...

# DNS

## Configuration de bind

J'ai suivi toutes les conf données dans les requis de DNS, pourtant impossible de lancer le serveur, aucune erreur dans `named.conf`.
J'air retenté en bloquant le firewalld, par intuition, mais ça n'a rien changé.

```bash
[root@localhost ~]# /usr/sbin/named-checkconf -z /etc/named.conf
/var/named/tp4.b2.db:1: SOA record not at top of zone (N.tp4.b2)
/var/named/tp4.b2.db:10: no TTL specified; zone rejected
/var/named/tp4.b2.db:12: no TTL specified; zone rejected
/var/named/tp4.b2.db:13: no TTL specified; zone rejected
/var/named/tp4.b2.db:14: no TTL specified; zone rejected
/var/named/tp4.b2.db:15: no TTL specified; zone rejected
/var/named/tp4.b2.db:16: no TTL specified; zone rejected
zone tp4.b2/IN: loading from master file /var/named/tp4.b2.db failed: not at top of zone
zone tp4.b2/IN: not loaded due to errors.
_default/tp4.b2/IN: not at top of zone
/var/named/20.5.10.db:1: SOA record not at top of zone (N.20.5.10.in-addr.arpa)
/var/named/20.5.10.db:10: no TTL specified; zone rejected
/var/named/20.5.10.db:13: no TTL specified; zone rejected
/var/named/20.5.10.db:16: no TTL specified; zone rejected
/var/named/20.5.10.db:17: no TTL specified; zone rejected
/var/named/20.5.10.db:18: no TTL specified; zone rejected
/var/named/20.5.10.db:19: no TTL specified; zone rejected
zone 20.5.10.in-addr.arpa/IN: loading from master file /var/named/20.5.10.db failed: not at top of zone
zone 20.5.10.in-addr.arpa/IN: not loaded due to errors.
_default/20.5.10.in-addr.arpa/IN: not at top of zone
zone localhost.localdomain/IN: loaded serial 0
zone localhost/IN: loaded serial 0
zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa/IN: loaded serial 0
zone 1.0.0.127.in-addr.arpa/IN: loaded serial 0
zone 0.in-addr.arpa/IN: loaded serial 0
```  

Il me rejete qu'il ne détecte pas la zone des guests.

## IPAM (Netbox)

Pour installer Netbox, il faut passer par epel-release, installer la derniure version de Python.
J'ai passer SELinux en permissive (recommander)

```bash
sudo setenforce 0
sudo sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

Après on doit installer PostgreSQL, qui est utilisé si j'ai bien compris pour sauvegarder les mots de passes.

```bash
[root@localhost ~]# /usr/pgsql-9.6/bin/postgresql96-setup initdb
Initializing database ... OK

[root@localhost ~]# sudo -u postgres psql

CREATE DATABASE netbox;
CREATE USER netbox WITH PASSWORD 'StrongPassword';
GRANT ALL PRIVILEGES ON DATABASE netbox TO netbox;
\q
```

Ensuite, dans `/opt/`, cloner le git Netbox `git clone -b master https://github.com/digitalocean/netbox.git`, copier `cp configuration.example.py configuration.py`
```
# Example: ALLOWED_HOSTS = ['netbox.example.com', 'netbox.internal.local']
ALLOWED_HOSTS = ['10.5.30.11']

# PostgreSQL database configuration.
DATABASE = {
    'NAME': 'netbox',                           # Database name
    'USER': 'netbox',                           # PostgreSQL username
    'PASSWORD': 'StrongPassword',               # PostgreSQL password
    'HOST': 'localhost',                        # Database server
    'PORT': '',                                 # Database port (leave blank for default)
}
```

